package com.techaspect.core.servlets;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.engine.SlingRequestProcessor;

import com.day.cq.contentsync.handler.util.RequestResponseFactory;
import com.day.cq.wcm.api.WCMMode;

// TODO: Auto-generated Javadoc
/**
 * The Class PDFUtil.
 */
public class PDFUtil {

	/**
	 * Determines if a byte array is compressed. The java.util.zip GZip
	 * implementation does not expose the GZip header so it is difficult to
	 * determine if a string is compressed.
	 *
	 * @param baos
	 *            the baos
	 * @return true, if is compressed
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static boolean isCompressed(ByteArrayOutputStream baos) throws IOException {
		byte[] bytes = baos.toByteArray();
		if ((bytes == null) || (bytes.length < 2)) {
			return false;
		} else {
			return ((bytes[0] == (byte) (GZIPInputStream.GZIP_MAGIC))
					&& (bytes[1] == (byte) (GZIPInputStream.GZIP_MAGIC >> 8)));
		}
	}

	/**
	 * Decompress content from server.
	 *
	 * @param contentBytes
	 *            the content bytes
	 * @return the byte[] of decompressed content
	 */
	public static byte[] decompress(byte[] contentBytes) {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {
			IOUtils.copy(new GZIPInputStream(new ByteArrayInputStream(contentBytes)), out);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return out.toByteArray();
	}

	/**
	 * Gets the string from path.
	 *
	 * @param filePath
	 *            the file path
	 * @param request
	 *            the request
	 * @param requestResponseFactory
	 *            the request response factory
	 * @param requestProcessor
	 *            the request processor
	 * @return the string from path
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static String getStringFromPath(String filePath, SlingHttpServletRequest request,
			RequestResponseFactory requestResponseFactory, SlingRequestProcessor requestProcessor)
			throws ServletException, IOException {
		HttpServletRequest req = requestResponseFactory.createRequest("GET", filePath);
		WCMMode.DISABLED.toRequest(req);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		HttpServletResponse resp = requestResponseFactory.createResponse(baos);
		requestProcessor.processRequest(req, resp, request.getResourceResolver());
		String output = "";
		if (isCompressed(baos)) {
			output = new String(decompress(baos.toByteArray()));
		} else {
			output = baos.toString("UTF-8");
		}
		return output;
	}
}
