package com.techaspect.core.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.engine.SlingRequestProcessor;

import com.day.cq.contentsync.handler.util.RequestResponseFactory;

/**
 *
 * This servlet is used to generate pdf from page.
 *
 * @author susheels
 */
@SlingServlet(methods = "GET", paths = "/bin/capella/servlet/generatePdf", metatype = false)
public class PdfGenerator extends SlingAllMethodsServlet {
	/** Default serial version id */
	private static final long serialVersionUID = 1L;

	/** The request response factory. */
	@Reference
	RequestResponseFactory requestResponseFactory;

	/** The request processor. */
	@Reference
	SlingRequestProcessor requestProcessor;

	/**
	 * @param request
	 *            SlingHttpServletRequest.
	 * @param response
	 *            SlingHttpServletResponse.
	 * @throws ServletException
	 *             the ServletException.
	 * @throws IOException
	 *             the IOException.
	 */
	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		// String reqOutput = "";
		PrintWriter out = response.getWriter();
		response.setCharacterEncoding("UTF-8");
		/* Setup request */
		String cssOutput = PDFUtil.getStringFromPath("/etc/designs/pdfgenerator/clientlib-all.css", request,
				requestResponseFactory, requestProcessor);
		out.print(cssOutput);
	}
}